module.exports = {
	title: 'Title',
	description: 'description',
	base: '/',
	head: [['link', { rel: 'icon', href: './public/stylechip-icon.png' }]],
	dest: '/',
	ga: '',
	markdown: {
		lineNumbers: true
	},

	themeConfig: {
		logo: "assets/img/stylechip-icon.png",
		nav: [
			{ text: 'Home', link: '/' },
			{ text: 'Guide', link: '/guide/' },
			{ text: 'GitLab', link: 'https://www.gitab.com/codecarrot/stylechip' }
		]
	}
}